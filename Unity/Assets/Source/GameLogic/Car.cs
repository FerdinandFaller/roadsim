﻿using UnityEngine;

namespace FF.RoadSim.GameLogic
{
    public abstract class Car
    {
        protected Driver Driver;
        protected int MaxSpeed;
        protected float Width;
        protected float Length;
        protected float Drag;
        protected Vector2 Velocity;
        protected Vector2 Position;
        protected float BrakeModifier;
        protected float BrakingIntensity;
        protected float AccelerationModifier;
        protected float AccelerationIntensity;


        protected Car(Driver driver)
        {
            Driver = driver;
        }

        public void Accelerate(float intensity)
        {
            AccelerationIntensity = intensity;
        }

        public void Brake(float intensity)
        {
            BrakingIntensity = intensity;
        }

        public void Update()
        {
            // Calculate the Velocity
        }
    }
}
