﻿namespace FF.RoadSim.GameLogic
{
    public class Driver
    {
        private readonly Car _car;
        private float _maxVisualRange;


        public Driver(Car car, float maxVisualRange)
        {
            _car = car;
            _maxVisualRange = maxVisualRange;
        }

        public void Accelerate(float intensity)
        {
            _car.Accelerate(intensity);
        }

        public void Brake(float intensity)
        {
            _car.Brake(intensity);
        }
    }
}
