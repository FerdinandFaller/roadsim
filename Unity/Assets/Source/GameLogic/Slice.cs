﻿namespace FF.RoadSim.GameLogic
{
    public class Slice
    {
        private const float Length = 1;

        private float _width;
        private int _radian;
        private float _visibilityModifier;
        private float _accelerationModifier;
        private float _brakeModifier;


        public Slice(float width, int radian, float visibilityModifier, float accelerationModifier, float brakeModifier)
        {
            _width = width;
            _radian = radian;
            _visibilityModifier = visibilityModifier;
            _accelerationModifier = accelerationModifier;
            _brakeModifier = brakeModifier;
        }
    }
}
