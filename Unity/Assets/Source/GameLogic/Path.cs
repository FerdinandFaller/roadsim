﻿using System.Collections.Generic;

namespace FF.RoadSim.GameLogic
{
    public class Path
    {
        private List<Slice> _slices;
        private List<Car> _cars;


        public Path()
        {
            _slices = new List<Slice>();
            _cars = new List<Car>();
        }
    }
}
